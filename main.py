from flask import Flask, redirect, request, render_template
import re
import yaml

# Configuration
domain = ["redirect", "example", "com"]
port = 9001

subdomain_regex = re.compile("^[^:/?#]+://([^?#]*)\.?" + "\.".join(domain))

app = Flask(__name__)


@app.route('/')
def redirect_subdomain():
    with open("subdomains.yaml", 'r') as stream:
        try:
            redirects = (yaml.load(stream))
        except yaml.YAMLError:
            return "I/O ERROR. REPORT TO ADMIN."

    subdomain = re.match(subdomain_regex, request.url).group(1).lower()[:-1]

    if subdomain and subdomain in redirects:
        return redirect(redirects[subdomain], code=302)
    else:
        return render_template("default.html", subdomain=subdomain)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port)
